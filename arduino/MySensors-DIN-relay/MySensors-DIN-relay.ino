/**
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2015 Sensnology AB
 * Full contributor list: https://github.com/mysensors/Arduino/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 *******************************
 *
 * REVISION HISTORY
 * Version 1.0 - Henrik Ekblad
 *
 * DESCRIPTION
 * Example sketch showing how to control physical relays.
 * This example will remember relay state after power failure.
 * http://www.mysensors.org/build/relay
 */

// Enable debug prints to serial monitor
//#define MY_DEBUG

// Enable and select radio type attached
#define MY_RADIO_NRF24
//#define MY_RADIO_RFM69

#define MY_RF24_CE_PIN 7
#define MY_RF24_CS_PIN 8
#define CHILD_ID1 0
#define CHILD_ID2 1

// Enable repeater functionality for this node
#define MY_REPEATER_FEATURE

#include <MySensors.h>

#define RELAY1_RESET    3
#define RELAY1_SET      4
#define RELAY2_RESET    5
#define RELAY2_SET      6
#define GREEN_LED       13
#define MANUAL_SWITCH1  10
#define MANUAL_SWITCH2  9

int button1State;             // the current reading from the input pin
int lastButton1State = LOW;   // the previous reading from the input pin
int button2State;             // the current reading from the input pin
int lastButton2State = LOW;   // the previous reading from the input pin

long lastDebounce1Time = 0;  // the last time the output pin was toggled
long lastDebounce2Time = 0;  // the last time the output pin was toggled
long debounceDelay = 50;    // the debounce time; increase if the output flickers
MyMessage msg1(CHILD_ID1, V_LIGHT);
MyMessage msg2(CHILD_ID2, V_LIGHT);

void relayOn(int relay)
{
  Serial.print("Relay ");
  Serial.print(relay);
  Serial.println(" ON");
  digitalWrite(relay == 1 ? RELAY1_SET : RELAY2_SET, HIGH);
  delay(50);
  digitalWrite(relay == 1 ? RELAY1_SET : RELAY2_SET, LOW);
}

void relayOff(int relay)
{
  Serial.print("Relay ");
  Serial.print(relay);
  Serial.println(" OFF");
  digitalWrite(relay == 1 ? RELAY1_RESET : RELAY2_RESET, HIGH);
  delay(50);
  digitalWrite(relay == 1 ? RELAY1_RESET : RELAY2_RESET, LOW);
}

void before()
{
  Serial.print("MYSENSORS DIN relay: ");
	// Then set relay pins in output mode
  pinMode(RELAY1_RESET, OUTPUT);
  pinMode(RELAY1_SET, OUTPUT);
  pinMode(RELAY2_RESET, OUTPUT);
  pinMode(RELAY2_SET, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  pinMode(MANUAL_SWITCH1, INPUT_PULLUP);
  pinMode(MANUAL_SWITCH2, INPUT_PULLUP);
  digitalWrite(GREEN_LED, 0);
	// Set relay to last known state (using eeprom storage)
  Serial.println("OK");
}

void setup()
{
}

void presentation()
{
  Serial.print("Presentation: ");
	// Send the sketch version information to the gateway and Controller
	sendSketchInfo("MYSENSORS plug", "1.0");
  present(CHILD_ID1, S_BINARY);
  present(CHILD_ID2, S_BINARY);
  Serial.println("OK");
  digitalWrite(GREEN_LED, 1);
}


void loop()
{
  static bool restored = false;

  if (!restored) {
    if (loadState(CHILD_ID1) == 1) {
      relayOn(1);
    }
    else {
      relayOff(1);
    }
    if (loadState(CHILD_ID2) == 1) {
      relayOn(2);
    }
    else {
      relayOff(2);
    }
    restored  = true;
  }
  int state1 = digitalRead(MANUAL_SWITCH1);
  if (state1 != lastButton1State) {
    lastDebounce2Time = millis();
  }
  int state2 = digitalRead(MANUAL_SWITCH2);
  if (state2 != lastButton2State) {
    lastDebounce2Time = millis();
  }
  if ((millis() - lastDebounce1Time) > debounceDelay) {
    if (state1 != button1State) {
      button1State = state1;
      if (button1State == LOW) {
        if (loadState(CHILD_ID1) == 0) {
          relayOn(1);
          saveState(CHILD_ID1, 1);
          send(msg1.set(1), 1);
        }
        else {
          relayOff(1);
          saveState(CHILD_ID1, 0);
          send(msg1.set(0), 1);
        }
      }
    }
  }
  lastButton1State = state1;
  if ((millis() - lastDebounce2Time) > debounceDelay) {
    if (state2 != button2State) {
      button2State = state2;
      if (button2State == LOW) {
        if (loadState(CHILD_ID2) == 0) {
          relayOn(2);
          saveState(CHILD_ID2, 1);
          send(msg2.set(1), 1);
        }
        else {
          relayOff(2);
          saveState(CHILD_ID2, 0);
          send(msg2.set(0), 1);
        }
      }
    }
  }
  lastButton2State = state2;
}

void receive(const MyMessage &message)
{
	// We only expect one type of message from controller. But we better check anyway.
  if (message.isAck()) {
     Serial.println("ACK from gateway");
  }
  else {
  	if (message.type == V_STATUS) {
  		// Change relay state
  		if (message.getBool() == 1) {
        relayOn(message.sensor);
  		}
      else {
        relayOff(message.sensor);
      }
  		// Store state in eeprom
  		saveState(message.sensor, message.getBool());
  		// Write some debug info
  		Serial.print("Incoming change for sensor:");
  		Serial.print(message.sensor);
  		Serial.print(", New status: ");
  		Serial.println(message.getBool());
  	}
  }
}

